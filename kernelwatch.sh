#!/bin/bash

cd /home/ceder/src/kernelwatch

PATH=/bin:/usr/bin:/usr/local/bin:/opt/bin:/usr/i686-pc-linux-gnu/gcc-bin/3.3.5:/opt/Acrobat5:/usr/X11R6/bin:/opt/blackdown-jdk-1.4.2.01/bin:/opt/blackdown-jdk-1.4.2.01/jre/bin:/usr/qt/3/bin:/usr/kde/3.3/bin:/usr/kde/3.2/bin:/usr/kde/3.1/bin:/usr/qt/2/bin:/usr/games/bin:/sbin:/usr/sbin:/sbin:/usr/sbin:/sbin:/usr/sbin
export PATH

wget -O - http://www.kernel.org/kdist/finger_banner > finger_banner || exit 1

# Process the filtered versions first.

cat finger_banner \
| grep -v -e '-rc[0-9]*$' \
| grep -v -e '-ac[0-9]*$' \
| grep -v -e '-mm[0-9]*$' \
| grep -v -e '-bk[0-9]*$' \
| grep -v -e '-git[0-9]*$' \
| grep -v -e '-pre[0-9]*$' \
> kw-filter.current
awk '{print $NF}' < kw-filter.current > version-filter.current

if [ `wc -l < version-filter.current` -lt 3 ]
then
    echo Got no sane data from web server >&2
    exit 1
fi

if [ `wc -l < version-filter.current` -gt 8 ]
then
    echo Got insanely much data from web server >&2
    exit 1
fi

diff -U 5 kw-filter.prev kw-filter.current > kw-filter.diff
status=$?
diff -U 0 version-filter.prev version-filter.current \
| sed 1,2d \
| sed -n -e 's/^\+//p' \
> version-filter.diff

if [ $status -eq 1 ]
then
    data=`pwd`
    (sed 1,3d kw-filter.diff
     echo
     echo Previous status change: `stat -c %y kw-filter.prev | sed 's/\..*//'`) \
    | (cd ~/rcvs/anon/python-lyskom && \
	 ./komsend --to="Filtrerad finger @kernel.org" \
	   --subject="`echo -n New kernels:' '; fmt $data/version-filter.diff`" \
	   2> $data/komsend-filter.out ) \
    || exit 1
    mv kw-filter.current kw-filter.prev
    mv version-filter.current version-filter.prev
fi

# Process the unfiltered versions.

cat finger_banner \
> kw.current
awk '{print $NF}' < kw.current > version.current

# Don't duplicate the sanity checks here.

diff -U 5 kw.prev kw.current > kw.diff
status=$?
diff -U 0 version.prev version.current \
| sed 1,2d \
| sed -n -e 's/^\+//p' \
> version.diff

if [ $status -eq 1 ]
then
    data=`pwd`
    (sed 1,3d kw.diff
     echo
     echo Previous status change: `stat -c %y kw.prev | sed 's/\..*//'`) \
    | (cd ~/rcvs/anon/python-lyskom && \
	 ./komsend --to="Finger @kernel.org" \
	   --subject="`echo -n New kernels:' '; fmt $data/version.diff`" \
	   2> $data/komsend.out ) \
    || exit 1
    mv kw.current kw.prev
    mv version.current version.prev
fi
