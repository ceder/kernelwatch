BEGIN {
  state = 0;
  category = "";
  fn = 0
}

FILENAME != fn {
  if (fn != 0)
  {
    for (i in entries) {
      old[i] = entries[i];
      delete entries[i];
    }
  }
  fn = FILENAME;
  state = 0;
  category = "";
}

/^-----*$/ {
  state = 1 - state
  next
}

state == 1 && /^IAB DOCUMENTS/ {
  category = gensub("..by date received.", "", $0) # iab
  next
}

state == 1 && /^WORKING GROUP STANDARDS TRACK/ {
  category = gensub("..by date received.", "", $0) # "wg-std"
  next
}

state == 1 && /^NON-WORKING GROUP STANDARDS TRACK/ {
  category = gensub("..by date received.", "", $0) # "non-wg-std"
  next
}

state == 1 && /^WORKING GROUP INFORMATION/ {
  category = gensub("..by date received.", "", $0) # "wg-info"
  next
}

state == 1 && /^NON-WORKING GROUP INFORMATION/ {
  category = gensub("..by date received.", "", $0) # "non-wg-info"
  next
}

state == 1 && /^INDEPENDENT/ {
  category = gensub("..by date received.", "", $0) # "indep"
  next
}

state == 1 {
  print "Unknown category heading found: " $0 > "/dev/stderr"
  exit 1
}

category == "" {
  next
}

$1 == "</PRE>" {
  state = 2
}

state == 2 {
  next
}

/^<a name=".*">$/ {
  name = gensub(/^<a name="(.*)">$/, "\\1", $0)
  entries[category, name] = "@category: " category "\n@draft: " name;
  next
}

/a href/ {
  re = "(.*)<a href=\"(.*)\">(.*)</a>";
  h_date = gensub(re, "\\1", $0);
  h_url = gensub(re, "\\2", $0);
  h_name = gensub(re, "\\3", $0);
  entries[category, name] = entries[category, name] "\n" h_name "\n<" h_url ">\n" h_date;
  next
}
  
{
  entries[category, name] = entries[category, name] "\n" $0;
}

END {

  need_heading = 1
  n = asorti(entries, sort_indir);
  for (i = 1; i <= n; i++) {
    split(sort_indir[i], separate, SUBSEP);
    category = separate[1];
    name = separate[2];
    if ((category, name) in old) {
    } else {
      if (need_heading) {
	print "New entries\n==========\n" > "head-rfcew.txt"
	need_heading = 0;
      }
      # print "NEW", category, name;
      print entries[category, name] > "head-rfcew.txt";
    }
  }

  need_heading = 1
  n = asorti(old, sort_indir);
  for (i = 1; i <= n; i++) {
    split(sort_indir[i], separate, SUBSEP);
    category = separate[1];
    name = separate[2];
    if ((category, name) in entries) {
    } else {
      if (need_heading) {
	print "Removed entries\n==============\n" > "head-rfcew.txt";
	need_heading = 0;
      }
      # print "REMOVED", category, name;
      print old[category, name] > "head-rfcew.txt";
    }
  }
  printf "" > "head-rfcew.txt";
  printf "" > "old-rfcew.txt";
  printf "" > "new-rfcew.txt";

  need_heading = 1
  n = asorti(entries, sort_indir);
  for (i = 1; i <= n; i++) {
    split(sort_indir[i], separate, SUBSEP);
    category = separate[1];
    name = separate[2];
    if ((category, name) in old && entries[category, name] != old[category, name]) {
      if (need_heading) {
	print "Changed entries\n===============\n" > "head-rfcew.txt";
	need_heading = 0;
      }
      # print "CHANGED", category, name;
      print old[category, name] > "old-rfcew.txt";
      print entries[category, name] > "new-rfcew.txt";
    }
  }

}
