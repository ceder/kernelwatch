#!/bin/bash

PATH=/bin:/usr/bin:/usr/local/bin:/opt/bin:/usr/i686-pc-linux-gnu/gcc-bin/3.3.5:/opt/Acrobat5:/usr/X11R6/bin:/opt/blackdown-jdk-1.4.2.01/bin:/opt/blackdown-jdk-1.4.2.01/jre/bin:/usr/qt/3/bin:/usr/kde/3.3/bin:/usr/kde/3.2/bin:/usr/kde/3.1/bin:/usr/qt/2/bin:/usr/games/bin:/sbin:/usr/sbin:/sbin:/usr/sbin:/sbin:/usr/sbin
export PATH

cd /home/ceder/rcvs/kernelwatch || exit 1

wget -q -O rfc-queue http://www.rfc-editor.org/queue.html || exit 1

rm -f head-rfcew.txt old-rfcew.txt new-rfcew.txt
awk -f rfc-editor-split.awk rfc-queue.old rfc-queue || exit 1
cat head-rfcew.txt > report-rfcew.txt
diff -U10000 old-rfcew.txt new-rfcew.txt | sed 1,3d >> report-rfcew.txt

if [ -s report-rfcew.txt ]
then
    cat report-rfcew.txt \
    | (cd ~/rcvs/anon/python-lyskom && \
	 ./komsend --to="RFC Editor Queue changes" \
	   --subject="RFC Editor queue changes" \
	   ) 2> komsend-rfcew.out \
    || exit 1
    mv -f rfc-queue rfc-queue.old
fi
